﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GeneralKnowledge.Test.App.Model.CsvProcessingTestModel
{
    public class Asset
    {
        public Guid AssetId { get; set; }

        [DisplayName("File name")]
        public string FileName { get; set; }

        [DisplayName("Mime type")]
        public string MimeType { get; set; }

        [DisplayName("Created by")]
        public string CreatedBy { get; set; }

        [DisplayName("E-mail")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
        public string Email { get; set; }

        [DisplayName("Country")]
        public string Country { get; set; }

        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}
