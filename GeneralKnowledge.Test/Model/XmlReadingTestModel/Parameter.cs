﻿using System.Xml.Serialization;

namespace GeneralKnowledge.Test.App.Model.XmlReadingTestModel
{
    public class Parameter
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlText]
        public double Value { get; set; }
    }
}