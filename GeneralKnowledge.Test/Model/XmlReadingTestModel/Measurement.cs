﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace GeneralKnowledge.Test.App.Model.XmlReadingTestModel
{
    public class Measurement
    {
        [XmlElement("param")]
        public List<Parameter> Parameters { get; set; }

        [XmlAttribute("date")]
        public DateTime Date { get; set; }
    }
}