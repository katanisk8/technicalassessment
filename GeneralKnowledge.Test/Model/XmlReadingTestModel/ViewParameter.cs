﻿namespace GeneralKnowledge.Test.App.Model.XmlReadingTestModel
{
    internal class ViewParameter
    {
        public string Name { get; set; }
        public double Low { get; set; }
        public double Avg { get; set; }
        public double Max { get; set; }
    }
}