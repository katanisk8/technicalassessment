﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace GeneralKnowledge.Test.App.Model.XmlReadingTestModel
{
    [XmlRoot("samples")]
    public class Samples
    {
        [XmlElement("measurement")]
        public List<Measurement> Measurements { get; set; }
    }
}
