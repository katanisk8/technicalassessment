﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using GeneralKnowledge.Test.App.Model.CsvProcessingTestModel;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            var csvFile = Resources.AssetImport;

            Stopwatch watchRegex = Stopwatch.StartNew();
            ReadByRegex(csvFile);
            watchRegex.Stop();
            
            Stopwatch watchLogic = Stopwatch.StartNew();
            IList<Asset> assets = ReadByLogic(csvFile);
            watchLogic.Stop();

            Console.WriteLine("Regex csv execution time: {0}", watchRegex.ElapsedMilliseconds);
            Console.WriteLine("Logic csv execution time: {0}", watchLogic.ElapsedMilliseconds);
        }

        public IList<Asset> GetAssets()
        {
            string csvFile = Resources.AssetImport;
            IList<Asset> assets = ReadByLogic(csvFile);

            return assets;
        }

        public IList<Asset> ReadByRegex(string csvFile)
        {
            IList<Asset> assets = new List<Asset>();

            using (StringReader reader = new StringReader(csvFile))
            {
                Regex regex = new Regex("((?<=\")[^\"]*(?=\"(,|$)+)|(?<=,|^)[^,\"]*(?=,|$))");
                string firstLine = reader.ReadLine();
                string nextLine = null;

                while ((nextLine = reader.ReadLine()) != null)
                {
                    string[] values = SplitLineByRegex(nextLine, regex);
                    assets.Add(CreateAsset(values));
                }
            }

            return assets;
        }

        public IList<Asset> ReadByLogic(string csvFile)
        {
            IList<Asset> assets = new List<Asset>();

            using (StringReader reader = new StringReader(csvFile))
            {
                string firstLine = reader.ReadLine();
                string nextLine = null;

                while ((nextLine = reader.ReadLine()) != null)
                {
                    string[] values = SplitLineByLogic(nextLine);
                    assets.Add(CreateAsset(values));
                }
            }

            return assets;
        }

        private static string[] SplitLineByRegex(string line, Regex regex)
        {
            MatchCollection matches = regex.Matches(line);
            string[] values = new string[matches.Count];

            for (int i = 0; i < matches.Count; i++)
            {
                values[i] = matches[i].Value;
            }

            return values;
        }

        private static string[] SplitLineByLogic(string line)
        {
            IList<string> results = new List<string>();
            StringBuilder result = new StringBuilder();
            bool inQuotes = false;

            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == '\"')
                {
                    inQuotes = !inQuotes;
                }
                else if (line[i] == ',')
                {
                    if (inQuotes == false)
                    {
                        results.Add(result.ToString());
                        result.Clear();
                    }
                    else
                    {
                        result.Append(line[i]);
                    }
                }
                else
                {
                    result.Append(line[i]);
                }
            }

            results.Add(result.ToString());

            return results.ToArray();
        }

        private static Asset CreateAsset(string[] values)
        {
            return new Asset
            {
                AssetId = GetAssetId(values[0]),
                FileName = values[1],
                MimeType = values[2],
                CreatedBy = values[3],
                Email = values[4],
                Country = values[5],
                Description = values[6]
            };
        }

        private static Guid GetAssetId(string assetId)
        {
            if (Guid.TryParse(assetId, out Guid candidate))
            {
                return candidate;
            }

            return Guid.Empty;
        }
    }
}
