﻿using System;
using System.Collections.Generic;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic string manipulation exercises
    /// </summary>
    public class StringTests : ITest
    {
        public void Run()
        {
            AnagramTest();
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {
            var word = "stop";
            var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };

            foreach (var possibleAnagram in possibleAnagrams)
            {
                Console.WriteLine(string.Format("{0} > {1}: {2}", word, possibleAnagram, possibleAnagram.IsAnagram(word)));
            }
        }

        private static void GetUniqueCharsAndCount()
        {
            var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";

            IDictionary<char, int> uniqueChars = word.GetUniqueCharsAndCount();
        }
    }

    public static class StringExtensions
    {
        public static bool IsAnagram(this string a, string b)
        {
            if (a.Length != b.Length)
            {
                return false;
            }

            IDictionary<char, int> aUniqueChars = a.GetUniqueCharsAndCount();
            IDictionary<char, int> bUniqueChars = b.GetUniqueCharsAndCount();

            foreach (char aKey in aUniqueChars.Keys)
            {
                if (bUniqueChars.ContainsKey(aKey) == false || 
                    aUniqueChars[aKey] != bUniqueChars[aKey])
                {
                    return false;
                }
            }

            return true;
        }

        public static IDictionary<char, int> GetUniqueCharsAndCount(this string word)
        {
            IDictionary<char, int> uniqueCharsAndCount = new Dictionary<char, int>();

            foreach (char _char in word)
            {
                if (uniqueCharsAndCount.Keys.Contains(_char))
                {
                    uniqueCharsAndCount[_char]++;
                }
                else
                {
                    uniqueCharsAndCount.Add(_char, 1);
                }
            }

            return uniqueCharsAndCount;
        }
    }
}
