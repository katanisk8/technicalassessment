﻿using ImageResizer;
using System;
using System.IO;
using System.Linq;
using System.Net;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        private readonly string _imageUrl = "https://c8.alamy.com/comp/BHX1Y8/world-cup-1982-group-a-poland-3-belgium-0-grzegorz-lato-of-poland-BHX1Y8.jpg";

        public void Run()
        {
            byte[] image = DownloadImage(_imageUrl);
            DrawNewImage(image, 100, 80, "thumbnail");
            DrawNewImage(image, 1200, 1600, "preview");
        }

        private static byte[] DownloadImage(string imageUrl)
        {
            WebRequest request = WebRequest.Create(imageUrl);
            WebResponse response = request.GetResponse();

            using (Stream stream = response.GetResponseStream())
            {
                if (stream != null)
                {
                    using (BinaryReader binaryReader = new BinaryReader(stream))
                    {
                        return binaryReader.ReadBytes(1000000);
                    }
                }
            }

            return null;
        }

        private static void DrawNewImage(byte[] image, int width, int height, string name)
        {
            if (image.Any())
            {
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, name);

                Instructions instructions = new Instructions();
                instructions.Width = width;
                instructions.Height = height;
                instructions.Format = "jpg";

                ImageJob imageJob = new ImageJob(image, path, instructions, false, true);

                ImageBuilder.Current.Build(imageJob);
            }
        }
    }
}
