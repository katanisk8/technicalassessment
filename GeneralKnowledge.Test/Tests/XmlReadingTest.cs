﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using GeneralKnowledge.Test.App.Model.XmlReadingTestModel;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// This test evaluates the 
    /// </summary>
    public class XmlReadingTest : ITest
    {
        public string Name { get { return "XML Reading Test"; } }

        public void Run()
        {
            var xmlData = Resources.SamplePoints;
            
            PrintOverview(xmlData);
        }

        private void PrintOverview(string xmlData)
        {
            Samples samples = Deserialize(xmlData);
            IList<ViewParameter> viewParameters = GetViewParameters(samples);

            Console.WriteLine();
            Console.WriteLine("{0,10}   {1,10}   {2,10}   {3,10}",
                "Parameter",
                "LOW",
                "AVG",
                "MAX");

            foreach (var viewParameter in viewParameters)
            {
                Console.WriteLine("{0,10}   {1,10}   {2,10:N2}   {3,10}",
                    viewParameter.Name,
                    viewParameter.Low,
                    viewParameter.Avg,
                    viewParameter.Max);
            }
        }

        private static Samples Deserialize(string xmlData)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Samples));

            using (StringReader reader = new StringReader(xmlData))
            {
                return (Samples)serializer.Deserialize(reader);
            }
        }

        private static IList<ViewParameter> GetViewParameters(Samples samples)
        {
            List<ViewParameter> viewParameters = new List<ViewParameter>();
            List<Parameter> xmlParameters = new List<Parameter>();

            xmlParameters.AddRange(samples.Measurements.SelectMany(x => x.Parameters));

            foreach (var paramName in xmlParameters.Select(x => x.Name).Distinct())
            {
                ViewParameter viewParameter = GetViewParameter(paramName, xmlParameters);
                viewParameters.Add(viewParameter);
            }

            return viewParameters;
        }

        private static ViewParameter GetViewParameter(string parameterName, IList<Parameter> xmlParameters)
        {
            IList<Parameter> selectedParameters = xmlParameters.Where(x => x.Name == parameterName).ToList();

            ViewParameter viewParameter = new ViewParameter();
            viewParameter.Name = parameterName;
            viewParameter.Low = selectedParameters.Min(x => x.Value);
            viewParameter.Avg = selectedParameters.Average(x => x.Value);
            viewParameter.Max = selectedParameters.Max(x => x.Value);

            return viewParameter;
        }
    }
}
