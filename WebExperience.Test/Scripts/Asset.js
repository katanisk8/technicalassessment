﻿viewModel = {
    assetCollection: ko.observableArray()
};

$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "/Assets/GetIndex",
    }).done(function (data) {
        $(data).each(function (index, element) {
            viewModel.assetCollection.push(element);
        });
        ko.applyBindings(viewModel);
    }).error(function (ex) {
        alert("Error");
    });
});