﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using GeneralKnowledge.Test.App.Model.CsvProcessingTestModel;
using WebExperience.Test.Models;
using PagedList;

namespace WebExperience.Test.Controllers
{
    public class AssetsController : Controller
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        public ActionResult Index(int? page = 1)
        {
            AssetsViewModel viewModel = new AssetsViewModel();
            int pageSize = 10;
            int pageNumber = page ?? 1;
            viewModel.Assets = _db.Assets.OrderBy(x => x.AssetId).ToPagedList(pageNumber, pageSize);

            return View(viewModel);
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Asset asset = _db.Assets.FirstOrDefault(x => x.AssetId == id.Value);

            if (asset == null)
            {
                return HttpNotFound();
            }

            return View(asset);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AssetId,FileName,MimeType,CreatedBy,Email,Country,Description")] Asset asset)
        {
            if (ModelState.IsValid)
            {
                asset.AssetId = GetAssetId();
                _db.Assets.Add(asset);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(asset);
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Asset asset = _db.Assets.FirstOrDefault(x => x.AssetId == id.Value);

            if (asset == null)
            {
                return HttpNotFound();
            }

            return View(asset);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AssetId,FileName,MimeType,CreatedBy,Email,Country,Description")] Asset asset)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(asset).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(asset);
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Asset asset = _db.Assets.FirstOrDefault(x => x.AssetId == id.Value);

            if (asset == null)
            {
                return HttpNotFound();
            }

            return View(asset);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Asset asset = _db.Assets.FirstOrDefault(x => x.AssetId == id);

            _db.Assets.Remove(asset);
            _db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }

            base.Dispose(disposing);
        }

        private Guid GetAssetId()
        {
            Guid candidate = Guid.NewGuid();

            if (_db.Assets.Any(x => x.AssetId == candidate))
            {
                return GetAssetId();
            }

            return candidate;
        }
    }
}
