﻿using GeneralKnowledge.Test.App.Model.CsvProcessingTestModel;
using PagedList;

namespace WebExperience.Test.Models
{
    public class AssetsViewModel
    {
        public IPagedList<Asset> Assets { get; set; }
    }
}