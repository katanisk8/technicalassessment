﻿using GeneralKnowledge.Test.App.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeneralKnowledge.Test.App;
using System.Collections.Generic;
using GeneralKnowledge.Test.App.Model.CsvProcessingTestModel;

namespace CsvProcessingUnitTests
{
    [TestClass]
    public class CsvProcessingUnitTests
    {
        [TestMethod]
        public void AreTheSameByRegexAndByLogic()
        {
            var csvProcessingTest = new CsvProcessingTest();
            var csvFile = Resources.AssetImport;

            var assetsByRegex = csvProcessingTest.ReadByRegex(csvFile);
            var assetsByLogic = csvProcessingTest.ReadByLogic(csvFile);

            AreTwoAssetsListsEqual(assetsByRegex, assetsByLogic);
        }

        private void AreTwoAssetsListsEqual(IList<Asset> firstList, IList<Asset> secondList)
        {
            Assert.AreEqual(firstList.Count, secondList.Count);

            for (int i = 0; i < firstList.Count; i++)
            {
                Assert.AreEqual(firstList[i].AssetId, secondList[i].AssetId);
                Assert.AreEqual(firstList[i].FileName, secondList[i].FileName);
                Assert.AreEqual(firstList[i].MimeType, secondList[i].MimeType);
                Assert.AreEqual(firstList[i].CreatedBy, secondList[i].CreatedBy);
                Assert.AreEqual(firstList[i].Email, secondList[i].Email);
                Assert.AreEqual(firstList[i].Country, secondList[i].Country);
                Assert.AreEqual(firstList[i].Description, secondList[i].Description);
            }
        }
    }
}
